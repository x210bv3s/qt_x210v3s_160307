## `S5PV210`  | `Linux-SDK`软件开发说明

---

时间：2023年12月12日13:24:58

[TOC]



## 1.资料

>1.[qt_x210v3s_160307: x210开发板的linux+QT的bsp (gitee.com)](https://gitee.com/x210bv3s/qt_x210v3s_160307)
>
>2.[项目管理 - PingCode](https://jmyl20230915231323500.pingcode.com/pjm/projects/S5PV210/backlog/6504ff0688f6abbf29a75118)
>
>3.[知识星球 | 深度连接铁杆粉丝，运营高品质社群，知识变现的工具 (zsxq.com)](https://wx.zsxq.com/dweb2/index/group/48884215481428)
>
>4.[3.Linux系统使用_透明水晶的博客-CSDN博客](https://blog.csdn.net/i_feige/category_6738190.html?spm=1001.2014.3001.5482)
>
>5.[5.ARM裸机_透明水晶的博客-CSDN博客](https://blog.csdn.net/i_feige/category_6738192.html?spm=1001.2014.3001.5482)

## 2.编译说明

```bash
[fly@752fac4b02e9 qt_x210v3s_160307]$ source build/envsetup.sh
[fly@752fac4b02e9 qt_x210v3s_160307]$ make help
=====================================================
make help                -> show make command info
make all                 -> Build All Project Code
make boot                -> build loader(uboot)
make kernel              -> build linux-kernel
make rootfs              -> build rootfs code
=====================================================
make clean               -> clean all
make boot_clean          -> clean loader(uboot)
make kernel_clean        -> clean linux-kernel
make rootfs_clean        -> clean rootfs code
=====================================================
```

