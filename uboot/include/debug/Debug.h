#ifndef __DEBUG_H__
#define __DEBUG_H__

//#include <stdio.h>
#include "color.h"
#include <common.h>


#define debug_msg(string...)   \
    do{\
        printf(NONE);\
        printf(string);\
        printf(NONE);\
    }while(0);

#define debug_msg_filter(filter, fmtstr, ...)   \
    do{\
        printf(NONE);\
        /*printf("filter: %s ,", filter);*/\
        printf(fmtstr);\
        printf(NONE);\
    }while(0);

#endif /* __DEBUG_H__ */
