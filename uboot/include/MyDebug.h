/***********************************************************
 *   > File Name: MyDebug.h
 *   > Author: fly
 *   > Create Time: 2023-06-17  6/24  16:39:21 +0800
 *========================================================*/
#ifndef __MYDEBUG_H__
#define __MYDEBUG_H__

#define CHKPNT  printf("%s()<%d>\n", __func__, __LINE__)


//===================================================================================
//#20200522#FLY@NOTE:
#define	DBG_PRINTF_FUNC		(1)
#define	DBG_PRINTF_SWITCH	(1)
#define	COLOR_DEBUG_FUNC	(1)

// color debug
#ifdef COLOR_DEBUG_FUNC
#define NONE		"\e[m"
#define RED			"\e[0;31m"
#define GREEN		"\e[0;32m"
#define YELLOW		"\e[0;33m"
#define BLUE		"\e[0;34m"
#else
#define NONE
#define RED
#define GREEN
#define YELLOW
#define BLUE
#endif

#ifdef DBG_PRINTF_FUNC
#if 1
#define	DBG_PRINTF(fmt, args...)	\
		do{\
		printf("<"BLUE"%s"NONE">("RED"%d"NONE" )["GREEN"%s"NONE"]", \
		strrchr(__FILE__, '/')?strrchr(__FILE__, '/')+1:__FILE__, __LINE__, __FUNCTION__);\
		printf(fmt, ##args);\
		}while(0)
#else
#define	DBG_PRINTF(fmt, args...)	\
			do{\
			printf("["GREEN"%s"NONE"]("YELLOW"%d"NONE")", __func__, __LINE__);\
			printf(fmt, ##args);\
			}while(0)
#endif

#define	DBG_PRINTF_K(key,fmt, args...)	\
		if(key){\
		printf("<<FILE:"BLUE"%s"NONE" LINE:"RED"%d"NONE" FUNC:"GREEN"%s"NONE">>", \
		strrchr(__FILE__, '/')?strrchr(__FILE__, '/')+1:__FILE__, __LINE__, __FUNCTION__);\
		printf(fmt, ##args);\
		}

#else
#define	DBG_PRINTF(fmt, args...)
#define	DBG_PRINTF_K(key,fmt, args...)
#endif /* DBG_PRINTF_FUNC */

#endif /* __MYDEBUG_H__  */
