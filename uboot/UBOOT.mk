.PHONY: help head uboot_clean uboot

UBOOT_TOP_DIR := $(shell pwd)

HOST_SYS := $(shell uname -s)
HOST_NAME := $(shell uname -n)
HOST_RELEASE := $(shell uname -r)
HOST_VERSION := $(shell uname -v)
GET_DATE         	:= $(shell date +%F)
GET_TIME         	:= $(shell date +%H:%M:%S)
GIT_SHA				:= $(shell git log -1 --pretty=format:"%H")
GIT_INFO			:= $(shell git log -1 --pretty=format:"%d <%an>")

HOST_FLAG	= \
			  -D_UBOOT_TOP_DIR_='"$(UBOOT_TOP_DIR)"' \
			  -D_HOST_NAME_='"$(HOST_NAME)"' \
			  -D_HOST_SYS_='"$(HOST_SYS)"' \
			  -D_HOST_RELEASE_='"$(HOST_RELEASE)"' \
			  -D_HOST_VERSION_='"$(HOST_VERSION)"' \
			  -D MAKE_TIME=\"$(GET_DATE)\ $(GET_TIME)\"  \
			  -D_GIT_SHA_='"$(GIT_SHA)"' \
			  -D_GIT_INFO_='"$(GIT_INFO)"' \

export UBOOT_TOP_DIR HOST_SYS HOST_NAME HOST_RELEASE HOST_VERSION HOST_FLAG

#MK_HEAD= ../../s5pv210-noos-dev/tools/mk_image/mkv210
MK_HEAD=sd_fusing/mkv210

help:
	@echo "This is help for uboot."	

uboot_clean:distclean
	@echo "make clean"
	
config:x210_sd_config
	@echo "make config"

head:
	@echo "make head for uboot."
	$(MK_HEAD) u-boot.bin

test:
	@echo $(MKCONFIG)
	@echo $(BUILD_DIR)
	@echo $(HOSTOS)
	@echo $(HOSTARCH)
	@echo $(HOST_FLAG)
	@echo $(UBOOT_TOP_DIR)
