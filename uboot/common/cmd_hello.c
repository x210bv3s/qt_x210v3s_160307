/*******************************************************************
 *   > File Name: cmd_hello.c
 *   > Author: fly
 *   > Create Time: 2020年10月23日 星期五 08时48分46秒
 *================================================================*/

#include <command.h>
#include <common.h>

#ifdef CONFIG_CMD_HELLO
int do_hello( cmd_tbl_t *cmdtp, int flag, int argc, char *argv[]){
    printf("Hello, This is my U-Boot command test. @fly\n");
    return 0;
}

/* -------------------------------------------------------------------- */

U_BOOT_CMD(
	hello,	1,	1,	do_hello,
	"hello  - test\n",
	NULL
);

#endif

