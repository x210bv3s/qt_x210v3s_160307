/*****************************************************
 *   > File Name: cmd_led.c
 *   > Author: fly
 *   > Create Time: 2021-07-26  1/30  10:05:16 +0800
 *==================================================*/
#include <command.h>
#include <common.h>

#ifdef CONFIG_CMD_LED
#include "led.c"

#define __MODULE__          main
#define __DBGLVL__ 6
#define __DBGFLT__ "*"      // *=All
#include <debug/DebugModule.h>

static int cmd_usage(cmd_tbl_t *cmdtp){
    DBG_WRN("%s\n", cmdtp->usage);
    return 0;
}

int init_flag = 0;
int do_led( cmd_tbl_t *cmdtp, int flag, int argc, char *argv[]){
    if (argc != 2){
        goto _error_;
    }

    if(!init_flag){
        DBG_MSG("led init\n");
        led_init();
        init_flag = 1;
    }

    if(!strcmp(argv[1], "on")){
        DBG_WRN("led on\n");
        led_set_all_led(1);
        return 0;
    }else if(!strcmp(argv[1], "off")){
        DBG_WRN("led off\n");
        led_set_all_led(0);
        return 0;
    }

    return 0;

_error_:
    DBG_ERR("need args.\n");
    return cmd_usage(cmdtp);
}

/* ----------------------------------------------------- */
U_BOOT_CMD(
	led,	2,	1,	do_led,
	"led  - test for led\n",
	"led [on/off]"
);
#endif /* CONFIG_CMD_LED */
