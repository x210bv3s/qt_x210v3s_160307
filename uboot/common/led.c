/*****************************************************
 *   > File Name: led.c
 *   > Author: fly
 *   > Create Time: 2021-07-26  1/30  10:19:24 +0800
 *==================================================*/
typedef struct{
    unsigned int GPD0CON;
    unsigned int GPD0DAT;
    unsigned int GPD0PUD;
    unsigned int GPD0DRV;
    unsigned int GPD0CONPDN;
    unsigned int GPD0PUDPDN;
}gpd0;
#define GPD0 (*(volatile gpd0*)0xE02000A0) 

typedef struct{
    unsigned int GPJ0CON;
    unsigned int GPJ0DAT;
    unsigned int GPJ0PUD;
    unsigned int GPJ0DRV;
    unsigned int GPJ0CONPDN;
    unsigned int GPJ0PUDPDN;
}gpj0;
#define GPJ0 (*(volatile gpj0*)0xE0200240) 

void led_init(void)
{
    /* Init LED1 GPIO, Def Off */
    GPJ0.GPJ0CON = (GPJ0.GPJ0CON & ~(0xf<<12)) | (0x1<<12);
    GPJ0.GPJ0DAT = (GPJ0.GPJ0DAT & ~(0x1<<3)) | (0x1<<3);

    /* Init LED2 GPIO, Def Off */
    GPJ0.GPJ0CON = (GPJ0.GPJ0CON & ~(0xf<<16)) | (0x1<<16);
    GPJ0.GPJ0DAT = (GPJ0.GPJ0DAT & ~(0x1<<4)) | (0x1<<4);

    /* Init LED3 GPIO, Def Off */
    GPJ0.GPJ0CON = (GPJ0.GPJ0CON & ~(0xf<<20)) | (0x1<<20);
    GPJ0.GPJ0DAT = (GPJ0.GPJ0DAT & ~(0x1<<5)) | (0x1<<5);

    /* Init LED4 GPIO, Def Off */
    GPD0.GPD0CON = (GPD0.GPD0CON & ~(0xf<<4)) | (0x1<<4);
    GPD0.GPD0DAT = (GPD0.GPD0DAT & ~(0x1<<1)) | (0x1<<1);
}

void led_set_led1(unsigned int value)
{
    if (value){
        GPJ0.GPJ0DAT = (GPJ0.GPJ0DAT & ~(0x1<<3)) | (0x0<<3);//ON
    }else{
        GPJ0.GPJ0DAT = (GPJ0.GPJ0DAT & ~(0x1<<3)) | (0x1<<3);//OFF
    }
}

void led_set_led2(unsigned int value)
{
    if (value){
        GPJ0.GPJ0DAT = (GPJ0.GPJ0DAT & ~(0x1<<4)) | (0x0<<4);//ON
    }else{
        GPJ0.GPJ0DAT = (GPJ0.GPJ0DAT & ~(0x1<<4)) | (0x1<<4);//OFF
    }
}

void led_set_led3(unsigned int value)
{
    if (value){
        GPJ0.GPJ0DAT = (GPJ0.GPJ0DAT & ~(0x1<<5)) | (0x0<<5);//ON
    }else{
        GPJ0.GPJ0DAT = (GPJ0.GPJ0DAT & ~(0x1<<5)) | (0x1<<5);//OFF
    }
}

void led_set_led4(unsigned int value)
{
    if (value){
        GPD0.GPD0DAT = (GPD0.GPD0DAT & ~(0x1<<1)) | (0x0<<1);//ON
    }else{
        GPD0.GPD0DAT = (GPD0.GPD0DAT & ~(0x1<<1)) | (0x1<<1);//OFF
    }
}

void led_set_all_led(unsigned int value)
{
    if(value){
        GPJ0.GPJ0DAT = (GPJ0.GPJ0DAT & ~(0x1<<3)) | (0x0<<3);//ON
        GPJ0.GPJ0DAT = (GPJ0.GPJ0DAT & ~(0x1<<4)) | (0x0<<4);//ON
        GPJ0.GPJ0DAT = (GPJ0.GPJ0DAT & ~(0x1<<5)) | (0x0<<5);//ON
        GPD0.GPD0DAT = (GPD0.GPD0DAT & ~(0x1<<1)) | (0x0<<1);//ON
    }else{
        GPJ0.GPJ0DAT = (GPJ0.GPJ0DAT & ~(0x1<<3)) | (0x1<<3);//OFF
        GPJ0.GPJ0DAT = (GPJ0.GPJ0DAT & ~(0x1<<4)) | (0x1<<4);//OFF
        GPJ0.GPJ0DAT = (GPJ0.GPJ0DAT & ~(0x1<<5)) | (0x1<<5);//OFF
        GPD0.GPD0DAT = (GPD0.GPD0DAT & ~(0x1<<1)) | (0x1<<1);//OFF
    }
}

static void delay(volatile unsigned int i)
{
    while(i--);
}

#define DELAY_COUNT     (900000)
void tester_flashled(void)
{
    led_init();

    for(;;){
        led_set_led1(1);
        delay(DELAY_COUNT);
        led_set_led1(0);
        delay(DELAY_COUNT);
    }
}
