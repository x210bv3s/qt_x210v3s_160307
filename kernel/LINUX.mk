###########################################################
  # File Name: LINUX.mk
  # Author: fly
  # Created Time: 2023-07-11  2/28  22:52:39 +0800
###########################################################

linux_clean:distclean
	@echo "clean"


linux_cfg:v210_defconfig
	@echo "config"

linux:
	@echo "build linux"



test:
	@echo "This is a test."
	@echo $(SUBARCH)
	@echo $(KERNELVERSION)

.PHONY: linux_clean linux_cfg linux
