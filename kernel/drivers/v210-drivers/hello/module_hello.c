/*****************************************************
 *   > File Name: module_hello.c
 *   > Author: fly
 *   > Create Time: 2021-08-13  5/32  09:56:10 +0800
 *==================================================*/

#include <linux/module.h>
#include <linux/init.h>

static int __init chrdev_init(void)
{
    printk(KERN_INFO "chardev_init hello world init.\n");

    return 0;
}

static void __exit chrdev_exit(void)
{
    printk(KERN_INFO "chrdev_exit hello world exit.\n");
}

module_init(chrdev_init);
module_exit(chrdev_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("flyer");
MODULE_DESCRIPTION("module test");
MODULE_ALIAS("alias xxx");
