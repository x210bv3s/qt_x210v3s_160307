#include <stdio.h>

//================================#20230427#FLY@For Debug=================================
#define	COLOR_DEBUG_FUNC	(1)

// color debug
#ifdef COLOR_DEBUG_FUNC
#define NONE		    "\e[m"
#define RED			    "\e[0;31m"
#define GREEN		    "\e[0;32m"
#define YELLOW		    "\e[0;33m"
#define BLUE		    "\e[0;34m"

#define LIGHT_RED       "\033[1;31m"
#define LIGHT_GREEN     "\e[1;32m"
#define LIGHT_GRAY      "\033[0;37m"    //高亮灰
#define LIGHT_BLUE      "\033[1;34;24m"
#define DARY_GRAY       "\033[1;30m"    //深灰
#define GYAN            "\033[0;36m"    //蓝绿色
#define LIGHT_CYAN      "\033[1;36m"
#define PURPLE          "\033[0;35m"
#define LIGHT_PURPLE    "\033[1;35m"
#define BROWN           "\033[0;33m"
#else
#define NONE
#define RED
#define GREEN
#define YELLOW
#define BLUE

#define LIGHT_RED       //"\033[1;31m"
#define LIGHT_GREEN     //"\e[1;32m"
#define LIGHT_GRAY      //"\033[0;37m"    //高亮灰
#define LIGHT_BLUE      //"\033[1;34;24m"
#define DARY_GRAY       //"\033[1;30m"    //深灰
#define GYAN            //"\033[0;36m"    //蓝绿色
#define LIGHT_CYAN      //"\033[1;36m"
#define PURPLE          //"\033[0;35m"
#define LIGHT_PURPLE    //"\033[1;35m"
#define BROWN           //"\033[0;33m"
#endif
//-----------------------------------------------------------------------------
// Compiler-time info
//-----------------------------------------------------------------------------


// Retrieve year info
#define OS_YEAR     ((((__DATE__ [7] - '0') * 10 + (__DATE__ [8] - '0')) * 10 \
					  + (__DATE__ [9] - '0')) * 10 + (__DATE__ [10] - '0'))

// Retrieve month info
#define OS_MONTH    (__DATE__ [2] == 'n' ? (__DATE__ [1] == 'a' ? 1 : 6) \
					 : __DATE__ [2] == 'b' ? 2 \
					 : __DATE__ [2] == 'r' ? (__DATE__ [0] == 'M' ? 3 : 4) \
					 : __DATE__ [2] == 'y' ? 5 \
					 : __DATE__ [2] == 'l' ? 7 \
					 : __DATE__ [2] == 'g' ? 8 \
					 : __DATE__ [2] == 'p' ? 9 \
					 : __DATE__ [2] == 't' ? 10 \
					 : __DATE__ [2] == 'v' ? 11 : 12)

// Retrieve day info
#define OS_DAY      ((__DATE__ [4] == ' ' ? 0 : __DATE__ [4] - '0') * 10 \
					 + (__DATE__ [5] - '0'))

// Retrieve hour info
#define OS_HOUR     ((__TIME__ [0] - '0') * 10 + (__TIME__ [1] - '0'))

// Retrieve minute info
#define OS_MINUTE   ((__TIME__ [3] - '0') * 10 + (__TIME__ [4] - '0'))

// Retrieve second info
#define OS_SECOND   ((__TIME__ [6] - '0') * 10 + (__TIME__ [7] - '0'))

#pragma once
#pragma message(GREEN"Compiling helloworld.c Begin..."NONE)

//#warning Unknown make?
//#error (see above here)

static void do_print_host_info(void)
{
	printf("\n==============================================================\n");
	printf("Build Time: "BLUE"%04d/%02d/%02d %02d:%02d:%02d"NONE"\n", \
		OS_YEAR, OS_MONTH, OS_DAY, OS_HOUR, OS_MINUTE, OS_SECOND);
	printf("SDK DIR   : %s\n", _PRJ_TOP_DIR_);
	printf("SDK CROSS : %s\n", _PRJ_CORSS_);
	printf("HOST INFO : %s %s %s\n", _HOST_SYS_, _HOST_NAME_, _HOST_RELEASE_);
	printf("HOST VER  : %s\n", _HOST_VERSION_);
	printf("GIT URL   : %s\n", _GIT_URL_);
	printf("GIT EMAIL : %s\n", _GIT_EMAIL_);
	printf("GIT SHA   : %s\n", _GIT_SHA_);
	printf("GIT INFO  : %s\n", _GIT_INFO_);
	printf("==============================================================\n");
}

int main(int argc, char *argv[])
{
	printf("hello, world.\n");

	do_print_host_info();
	return 0;
}
