#################################################################
#
# lcdtester
#
#################################################################

APP_LCDTESTER_VERSION=1.0.0
APP_LCDTESTER_SITE_METHOD:=local
APP_LCDTESTER_SITE=$(CURDIR)/app/lcdtester
APP_LCDTESTER_INSTALL_TARGET:=YES

define	APP_LCDTESTER_BUILD_CMDS
	$(MAKE) CC="$(TARGET_CC)" LD="$(TARGET_LD)" -C $(@D) all
endef

define	APP_LCDTESTER_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 0755 $(@D)/lcdtester $(TARGET_DIR)/bin
endef

define APP_LCDTESTER_PERMISSIONS
	/bin/lcdtester f 4755 0 0 - - - -
endef

$(eval $(generic-package))
