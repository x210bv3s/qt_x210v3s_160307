###################################################################
#
# Makefile -- Top level makefile.
#
# Copyright (c) 20123/12/07
#
  # File Name: Makefile
  # Author: fly
  # Mail: 
  # Created Time: Thu 07 Dec 2023 01:36:03 PM CST
###################################################################
# color printf
BR_FW="\e[1;41m"
BG_FW="\e[1;42m"
BY_FW="\e[1;43m"
BB_FW="\e[1;44m"
NONE="\e[0m"
GREEN="\e[1;42m"

# common
MULTI_CORES ?= $(shell grep -c ^processor /proc/cpuinfo)
SHELL = /bin/bash
PYTHON := python
TOP_PWD = $(shell pwd)

PRJ_TOP_DIR 		  := $(shell pwd)
HOST_SYS 			  := $(shell uname -s)
HOST_NAME 			  := $(shell uname -n)
HOST_RELEASE 		  := $(shell uname -r)
HOST_VERSION 		  := $(shell uname -v)
GET_DATE              := $(shell date +%F)
GET_TIME              := $(shell date +%H:%M:%S)
GIT_SHA				  := $(shell git log -1 --pretty=format:"%H")
GIT_INFO			  := $(shell git log -1 --pretty=format:"%d <%an>")
GIT_URL				  := $(strip $(shell cat .git/config | grep "url" | awk -F'samoon' '{print $$NF;}'))
GIT_EMAIL			  := $(shell git config user.email)

HOST_FLAG	= \
			  -D_PRJ_TOP_DIR_='"$(PRJ_TOP_DIR)"' \
			  -D_HOST_NAME_='"$(HOST_NAME)"' \
			  -D_HOST_SYS_='"$(HOST_SYS)"' \
			  -D_HOST_RELEASE_='"$(HOST_RELEASE)"' \
			  -D_HOST_VERSION_='"$(HOST_VERSION)"' \
			  -D MAKE_TIME=\"$(GET_DATE)\ $(GET_TIME)\"  \
			  -D_GIT_SHA_='"$(GIT_SHA)"' \
			  -D_GIT_INFO_='"$(GIT_INFO)"' \
			  -D_PRJ_CFG_='"$(NVT_PRJCFG_CFG)"' \
			  -D_PRJ_MODEL_='"$(NVT_PRJCFG_MODEL_CFG)"' \
			  -D_PRJ_CORSS_='"$(NVT_CROSS)"' \
			  -D_GIT_URL_='"$(GIT_URL)"' \
			  -D_GIT_EMAIL_='"$(GIT_EMAIL)"'

export PRJ_TOP_DIR HOST_SYS HOST_NAME HOST_RELEASE HOST_VERSION HOST_FLAG

.PHONY: test env checkenv uboot boot boot_clean kernel kernel_clean help  all

all: boot kernel rootfs

checkenv:
	@if [ -z $(SDK_BUILD_TOP) ]; then \
		echo -e "\r\nERROR :Please source build/envsetup.sh in qt_x210v3s_160307 firstly to have auto copyso function\r\n"; \
		exit 1; \
	fi
	@if [ $(TOP_PWD) != $(SDK_BUILD_TOP) ]; then \
		echo -e "\r\nERROR: Current path is not the same as the environment path ("$(SDK_BUILD_TOP)")\r\n"; \
		exit 1; \
	fi
	@if [ ! -e $(OUTPUT_DIR) ]; then \
		mkdir $(OUTPUT_DIR); \
	fi

test:checkenv
	@echo -e $(GREEN)"This is a test\e[0m"
	@echo ${HOST_FLAG}

# log
log_stdout = echo -e "\e[1;44m$@: Build start\e[0m"; \
		$(1) 2> >(tee -a $(LOGS_DIR)/$@.log_err) > >(tee -a $(LOGS_DIR)/$@.log); \
		ret=$$PIPESTATUS; \
		echo -e "\e[1;44m$@: Build finish\e[0m"; \
		cat $(LOGS_DIR)/$@.log_err >&2; \
		if [ -z "`cat $(LOGS_DIR)/$@.log_err`" ]; then \
			rm $(LOGS_DIR)/$@.log_err; \
		fi; \
		exit $$ret;
log_remove = if [ ! -e $(LOGS_DIR) ]; then mkdir $(LOGS_DIR); fi; if [ -e $(LOGS_DIR)/$@.log ]; then rm $(LOGS_DIR)/$@.log; fi; if [ -e $(LOGS_DIR)/$@.log_err ]; then rm $(LOGS_DIR)/$@.log_err; fi;

# misc
BUILD_COMPLETE_STRING ?= $(shell date "+%a, %d %b %Y %T %z")
UID := $(shell id -u)
GID := $(shell id -g)
removeimg = $(foreach a,$(1),$(if $(wildcard $(a)),rm -rf $(a)))
#unexport KBUILD_OUTPUT

fw_rename = echo -e "\e[1;44m$@: Copy Firmware For Customer\e[0m"; \
		cp ${MSTART_IPL} ${MSTART_OUTPUT_IPL};\
		cp ${MSTART_IPL_CUST} ${MSTART_OUTPUT_IPL_CUST};\
		cp ${MSTART_UBOOT} ${MSTART_OUTPUT_UBOOT};\
		cp ${MSTART_FW} ${MSTART_OUTPUT_FW};\
		echo -e $(GREEN)"$@: Copy Firmware For Completed.\e[0m";

#output:checkenv
#	@$(call fw_rename)

####uboot: checkenv
####	@echo "##### Build u-boot loader #####"
####	@$(call log_remove)
####	@$(call log_stdout, make -C $(UBOOT_DIR) O="" distclean)
####	@$(call log_stdout, make -C $(UBOOT_DIR) O="" nvt-na51023_config)
####	@$(call log_stdout, make -C $(UBOOT_DIR) O="")
####	@$(call log_stdout, make -C $(UBOOT_DIR) O="" env)
####	@$(UBOOT_DIR)/tools/encrypt_bin SUM $(UBOOT_DIR)/u-boot.bin 0x350 ub51023
####	@$(BUILD_DIR)/nvt-tools/bfc c lz $(UBOOT_DIR)/u-boot.bin $(UBOOT_DIR)/u-boot.lz.bin 0 0
####	@cp -af $(UBOOT_DIR)/u-boot.bin $(UBOOT_DIR)/u-boot.lz.bin $(OUTPUT_DIR)/

boot: checkenv
	@echo "##### Build boot loader #####"
	@make -C $(BOOT_DIR) O="" clean -j$(MULTI_CORES)
	@make -C $(BOOT_DIR) O="" config
	@make -C $(BOOT_DIR) O="" -j$(MULTI_CORES)
	@echo "##### Copy boot loader #####"
	@make -C $(BOOT_DIR) head
	@cp -rf ${BOOT_DIR}/u-boot.bin $(OUTPUT_DIR)

boot_clean: checkenv
	@echo "##### Clean boot loader #####"
	@make -C $(BOOT_DIR) O="" clean -j$(MULTI_CORES)

kernel: checkenv
	@echo "##### Build kernel #####"
	@make -C $(KERNEL_DIR) O="" distclean
	@echo ">>>>> build kernel with $(SDK_KERNEL_CFG) <<<<<"
	@make -C $(KERNEL_DIR) O="" $(SDK_KERNEL_CFG)
	@make -C $(KERNEL_DIR) O="" -j$(MULTI_CORES)
	@echo -e ${GREEN}"##### Copy IMG To Trage Dir #####"${NONE}
	@cp -rf ${SDK_KERNEL_IMG} $(OUTPUT_DIR)

kernel_clean: checkenv
	@echo "##### Clean kernel #####"
	@make -C $(KERNEL_DIR) O="" clean

rootfs: checkenv
	@echo "##### Build Rootfs #####"
	@echo ">>>>> build Rootfs with $(SDK_ROOTFS_CFG) <<<<<"
	@make -C $(SDK_ROOTFS_DIR) $(SDK_ROOTFS_CFG)
	@make -C $(SDK_ROOTFS_DIR) -j$(MULTI_CORES)
	@cp -v ${SDK_BUILD_TOP}/buildroot/output/images/rootfs.tar ${OUTPUT_DIR}

rootfs_clean: checkenv
	@echo "##### Clean Rootfs #####"
	@make -C $(SDK_ROOTFS_DIR) clean

clean: rm_logs boot_clean kernel_clean rootfs_clean
	@echo "make clean completed"

rm_logs:
	$(call removeimg,$(LOGS_DIR))

showenv:
	@echo $(PATH)

help: 
	@echo "====================================================="
	@echo "make help                -> show make command info"
	@echo "make all                 -> Build All Project Code"
	@echo "make boot                -> build loader(uboot)"
	@echo "make kernel              -> build linux-kernel"
	@echo "make rootfs              -> build rootfs code"
	@echo "====================================================="
	@echo "make clean               -> clean all"
	@echo "make boot_clean          -> clean loader(uboot)"
	@echo "make kernel_clean        -> clean linux-kernel"
	@echo "make rootfs_clean        -> clean rootfs code"
	@echo "====================================================="
